from rllab.envs.base import Env
from rllab.envs.base import Step
from rllab.spaces import Box
import numpy as np
from keras.models import load_model
import pickle


def load_scaler(scaler):
    if scaler is None:
        return None
    if isinstance(scaler, str):
        with open(scaler, 'rb') as pf:
            scaler = pickle.load(pf)
    return scaler


class HumanMuscles(Env):

    def __init__(self, model='name_mlp.h5',
                 scaler_model_in=None, scaler_model_out=None,
                 tar_angle=130, ini_angle=120, ini_vel=0, ini_acc=0, pw_range='0350'):
        if isinstance(model, str):
            self.model = load_model(model)
        else:
            self.model = model
        self.scaler_model_in = load_scaler(scaler_model_in)
        self.scaler_model_out = load_scaler(scaler_model_out)
        self.tar_angle = tar_angle
        self.ini_angle = ini_angle
        self.ini_vel = ini_vel
        self.ini_acc = ini_acc
        self.picked_action = 0
        self.inputs = np.zeros(7, dtype=float)
        self._state = []
        self.delaPos = self.tar_angle - self.ini_angle
        self.pw_range = pw_range

    @property
    def observation_space(self):
        return Box(low=np.array([50, -1, -0.01, 0]), high=np.array([180, 1, 0.01, 180]),
                   shape=None)

    @property
    def action_space(self):
        return Box(low=np.array([-1, -1]), high=np.array([1, 1]), shape=None)  # tanh activation

    def reset(self):
        self.inputs = np.array([self.ini_angle, self.ini_vel, self.ini_acc, 0, 0], dtype=float)
        self._state = np.array([self.ini_angle, self.ini_vel, self.ini_acc, self.delaPos], dtype=float)
        observation = np.copy(self._state)
        return observation

    def step(self, action):
        
        # Define the interval of pulse width values
        if self.pw_range == '250350':
            self.picked_action = action * 50 + 300  # tanh activation [250-350]
        elif self.pw_range == '100350':
            self.picked_action = action * 125 + 225  # tanh activation [100-350]
        elif self.pw_range == '200350':
            self.picked_action = action * 75 + 275  # tanh activation [200-350]
        else:
            self.picked_action = action * 175 + 175  # tanh activation [0-350]

        self.picked_action = np.array(self.picked_action)
        sa_dim = 7
        sa = np.hstack([self.inputs, self.picked_action])
        if self.scaler_model_in is not None:
            sa = self.scaler_model_in.transform(sa.reshape(-1, sa_dim))

        # State transition
        nexts_hat = self.model.predict(sa)
        if self.scaler_model_out is not None:
            self._state[:3] = self.scaler_model_out.inverse_transform(nexts_hat)

        self.inputs[:3] = self._state[:3]
        self._state[3] = self.tar_angle - self.inputs[0]

        # Computing the history of stimulation
        self.inputs[3] = self.inputs[3] + self.picked_action[0]  # fat21
        if self.picked_action[0] == 0:
            self.inputs[3] = 0
        self.inputs[4] = self.inputs[4] + self.picked_action[1]  # fat22
        if self.picked_action[1] == 0:
            self.inputs[4] = 0

        # Reward function
        reward = - (self.tar_angle - self.inputs[0]) ** 2  # Continuous

        done = False
        next_observation = np.copy(self._state)

        return Step(observation=next_observation, reward=reward, done=done)