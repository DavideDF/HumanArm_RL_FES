from rllab.algos.ppo import PPO
from rllab.envs.human_muscles import HumanMuscles
from rllab.baselines.linear_feature_baseline import LinearFeatureBaseline
from rllab.policies.gaussian_mlp_policy import GaussianMLPPolicy
import random
from scipy import io
import lasagne.nonlinearities as NL
import numpy as np
import matplotlib.pyplot as plt

'''SETTINGS''' 
initial_position = 120
set_of_targets = [130, 140, 160]
pw_range = '200350'  # Set the interval of pulse width values 
nn = 12  # number of hidden layers units
episode_duration = 60
n_eps_x_iteration = 50

# SELECT THE MODEL
# model_name = 'ann9relu_sep'
model_name = 'ann13relu_ranPW'


scal_in = '/../rl_fes/models/' + model_name + '_in_scaler.pickle'
scal_out = '/../rl_fes/models/' + model_name + '_out_scaler.pickle'
model = '/../rl_fes/models/' + model_name + '_mlp.h5'
ci = pd.read_csv('/../rl_fes/models/ci_selcted_' + model_name + '.csv',
 delim_whitespace=False)
set_of_ci = ci.values


if __name__ == "__main__":

    def run_task(*_):
        env = HumanMuscles(scaler_model_in=scal_in,
                                       scaler_model_out=scal_out,
                                       model=model,
                                       ini_angle=initial_position,
                                       pw_range=pw_range)

        policy = GaussianMLPPolicy(env_spec=env.spec,
                                   hidden_sizes=(nn, nn),
                                   # learn_std=False,
                                   # init_std=0.0316,
                                   hidden_nonlinearity=NL.tanh,
                                   output_nonlinearity=NL.tanh)
        std_s_vct = []
        baseline = LinearFeatureBaseline(env_spec=env.spec)

        std_s_vct.append(policy.get_param_values(trainable=True)[-2:])

        batch_size = episode_duration * n_eps_x_iteration
        n_iterations = 250 * len(set_of_targets)

        for its in range(n_iterations):
            print('ITERATION N ' + str(its) + ' of ' + str(n_iterations))
            env.tar_angle = random.choice(set_of_targets)
            
            '''# Uncomment if you want to randomly change the initial conditions
            ci_index = random.choice(range(0, len(set_of_ci)))
            env.ini_angle = set_of_ci[ci_index, 0]
            env.ini_vel = set_of_ci[ci_index, 1]
            env.ini_acc = set_of_ci[ci_index, 2]
            '''

            algo = PPO(env=env,
                       policy=policy,
                       baseline=baseline,
                       batch_size=batch_size,
                       max_path_length=episode_duration,
                       n_itr=1,
                       discount=0.99,
                       step_size=0.01,
                       # plot=True,
                       )

            algo.train()

            std_s_vct.append(policy.get_param_values(trainable=True)[-2:])

        weigh = policy.get_param_values(trainable=True)

        policy_name = model_name + '_' + pw_range \
                      + '_' + str(nn) + '_' +\
                      str(initial_position) + '_' +\
                      str(set_of_targets[0]) + str(set_of_targets[1]) + str(set_of_targets[2])

        io.savemat('/../rl_fes/policies/' + policy_name, mdict={'policy_parameters': weigh})

        # plot std estimation parameters
        std_s_vct = np.squeeze(std_s_vct)
        plt.figure()
        plt.plot(std_s_vct[:, 0])
        plt.plot(std_s_vct[:, 1])
        plt.title(policy_name)
        plt.ylabel('Std Estimation Parameters')
        plt.xlabel('Iteration')
        plt.show()

    run_task()