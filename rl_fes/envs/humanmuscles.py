import gym
from gym import spaces
from gym.utils import seeding
from keras.models import load_model
import numpy as np
import pickle


def load_scaler(scaler):
    if scaler is None:
        return None
    if isinstance(scaler, str):
        with open(scaler, 'rb') as pf:
            scaler = pickle.load(pf)
    return scaler


class HumanMusclesEnv(gym.Env):
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 30
    }

    def __init__(self, model='name_mlp.h5',
                 scaler_model_in=None, scaler_model_out=None,
                 target_angle=100, init_angle=120, num_out_states=3):
        """Construct a human muscles simulator.

            Args:
                model (str, object): Object implementing the transition model
                scaler_model_in (None, str, object): Object used to normalize the
                                                    input of the model
                scaler_model_out (None, str, object): Object used to de-normalize
                                                    the output of the model
                action_type (str): how action should be interpreted
                target_angle: Desired angular position
                num_out_states: State dimension

        """
        if isinstance(model, str):
            self.model = load_model(model)
        else:
            self.model = model

        self.scaler_model_in = load_scaler(scaler_model_in)
        self.scaler_model_out = load_scaler(scaler_model_out)

        # State upper and lower bound
        self.low_state = np.array([50, -1, -0.01, 0, 0])
        self.high_state = np.array([180, 1, 0.01, 50000, 50000])

        self.viewer = None
        self.observation_space = spaces.Box(self.low_state, self.high_state)
        self.num_out_states = num_out_states

        self.target_angle = target_angle
        self.init_angle = init_angle

        # Initialize and reset
        self._seed()
        self.reset()

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _step(self, action):
        state_dim = self.observation_space.shape[0]
        model_action_dim = 2
        sa_dim = state_dim + model_action_dim

        # State-action pair
        action = np.squeeze(action)
        sa = np.hstack([self.state, action])
        if self.scaler_model_in is not None:
            sa = self.scaler_model_in.transform(sa.reshape(-1, sa_dim))

        # State transition
        nexts_hat = self.model.predict(sa)
        if self.scaler_model_out is not None:
            nexts_hat = self.scaler_model_out.inverse_transform(nexts_hat)

        # Next state definition
        next_state = np.empty(state_dim)
        # next_state[0:3] = self.state[0:3]
        next_state[0:3] = nexts_hat

        # Computing the new history status
        next_state[3] = self.state[3] + action[0]  # fat21
        if action[0] == 0:
            next_state[3] = 0
        next_state[4] = self.state[4] + action[1]  # fat22
        if action[1] == 0:
            next_state[4] = 0

        # Bounds on states
        next_state = np.clip(next_state, self.low_state, self.high_state)

        # Reward function
        reward = - (self.target_angle - next_state[0]) ** 2  # - abs(next_state[2]) * 10**4

        # Terminating point (none)
        done = False

        self.state = next_state
        action_vect = action
        return np.array(self.state[:self.num_out_states]), reward, done, action_vect, {}

    def _reset(self, state=None):
        if state is None:
            self.state = np.array(
                [self.init_angle, 0, 0, 0, 0], dtype=np.float32)
        else:
            self.state = state
        return np.array(self.state)

    def _render(self, mode='human', close=False):
        pass
