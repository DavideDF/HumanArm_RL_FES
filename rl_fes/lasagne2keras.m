function [l0, l1, l2] = lasagne2keras(arch, w_vect)

% ------------------------------------------------------------------------
% Convert the policy parameters obtained by using lasagne (rllab codes),
% saved in matlab format, in keras format (in csv).
%
% arch: network arkitecture (e.g. [22 22]).
% w_vect: one-dimensional vector containing all the weights.
% l0: list of weights and bias of the first layer.
% l1: list of weights and bias of the second layer.
% l2: list of weights and bias of the output layer.
% The inputs are set to 4.
% ------------------------------------------------------------------------

w0=zeros(4,arch(1));

w1=zeros(arch(1),arch(2));

w2=zeros(arch(2),2);

ind=1;

w0(1,:)=w_vect(ind:arch(1));
ind=ind+arch(1);
w0(2,:)=w_vect(ind:ind-1+arch(1));
ind=ind+arch(1);
w0(3,:)=w_vect(ind:ind-1+arch(1));
ind=ind+arch(1);
w0(4,:)=w_vect(ind:ind-1+arch(1));
ind=ind+arch(1);
b0=w_vect(ind:ind-1+arch(1));
ind=ind+arch(1);

for ii=1:arch(1)
    w1(ii,:)=w_vect(ind:ind-1+arch(2));
    ind=ind+arch(2);
end
b1=w_vect(ind:ind-1+arch(2));
ind=ind+arch(2);

for ii=1:arch(2)
    w2(ii,:)=w_vect(ind:ind-1+2);
    ind=ind+2;
end
b2=w_vect(ind:ind-1+2);

l0={w0; b0};
l1={w1; b1};
l2={w2; b2};

save('l0.mat', 'l0');
save('l1.mat', 'l1');
save('l2.mat', 'l2');
