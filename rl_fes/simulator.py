import numpy as np
from envs.humanmuscles import HumanMusclesEnv
from utils import multip_plotter
from keras.models import Sequential
from keras.layers import Dense
from scipy.io import loadmat


# Define the policy
nn = 14
dense_layer_sizes = [nn, nn]
activation = 'tanh'
policy = Sequential()
policy.add(Dense(dense_layer_sizes[0],
                 activation=activation, input_dim=4))
for layer_size in dense_layer_sizes[1:]:
    policy.add(Dense(layer_size, activation=activation))
policy.add(Dense(2, activation=activation))

# Load the matlab weights, obtained with lasagne2keras.m
l0 = np.squeeze(loadmat('l0.mat').values()[2])
l0[1] = np.squeeze(l0[1])
l1 = np.squeeze(loadmat('l1.mat').values()[2])
l1[1] = np.squeeze(l1[1])
l2 = np.squeeze(loadmat('l2.mat').values()[1])
l2[1] = np.squeeze(l2[1])

# Set the policy parameters
policy.layers[0].set_weights(l0)
policy.layers[1].set_weights(l1)
policy.layers[2].set_weights(l2)

# Load the subject's arm model.
# model_name = 'ann13relu_ranPW'
model_name = 'ann9relu_sep'
scal_in = '/../rl_fes/models/' + model_name + '_in_scaler.pickle'
scal_out = '/../rl_fes/models/' + model_name + '_out_scaler.pickle'
model = '/../rl_fes/models/' + model_name + '_mlp.h5'

# Set subject's stimulation currents

initial_position = 120
target_position = 140
duration = 60

env = HumanMusclesEnv(scaler_model_in=scal_in,
                      scaler_model_out=scal_out,
                      model=model,
                      init_angle=initial_position)

if __name__ == '__main__':

    target_trajectory = target_position * np.ones(shape=duration)  # constant ref.

    state = np.hstack((env.reset()[:3], (target_trajectory[0] - initial_position)))
    reward_sum = 0
    action_old = 0
    next_state = state
    simout = []
    for t in range(len(target_trajectory)):
        state[3] = target_trajectory[t] - next_state[0]
        env.target_angle = target_trajectory[t]
        action = np.squeeze(policy.predict(np.array(state).reshape(1, 4), batch_size=1))

	# Set the pulse width interval
        action = action * 175 + 175  # tanh activation [0-350]
        # action = action * 125 + 225  # tanh activation [100-350]
        # action = action * 100 + 250  # tanh activation [150-350]
        # action = action * 75 + 275  # tanh activation [200-350]
        # action = action * 50 + 300  # tanh activation [250-350]

        # Compute the state-transition
        next_state, reward, _, currents, _ = env.step(action)

        simout.append(np.hstack([next_state, currents]))
        reward_sum += reward

        state[:3] = next_state
        action_old = action

    simout = np.asarray(simout)

    # Plot trajectory and control action
    multip_plotter(simout, target_trajectory)
    print('Last Angle: %.2f' % next_state[0], 'Sum of Collected Rewards: %.2f' % reward_sum)
