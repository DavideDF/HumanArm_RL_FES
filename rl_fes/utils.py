import matplotlib.pyplot as plt
import numpy as np
import math


def plotter(simout, rewards=None):

    plt.figure()
    plt.subplot(211)
    plt.plot(simout[:, 0])
    plt.title('Elbow Position')
    plt.ylabel('Angle [deg]')

    plt.subplot(212)
    plt.stem(simout[:, 3], 'r', markerfmt=" ", label='current on ch1')
    plt.stem(simout[:, 4], 'b', markerfmt=" ", label='current on ch2')
    plt.title('Currents')
    plt.xlabel('instants')
    plt.ylabel('Amplitude [mA]')
    plt.legend()
    '''
    plt.figure()
    plt.subplot(311)
    plt.plot(simout[:, 0])
    plt.title('Elbow Position')
    plt.ylabel('[deg]')
    plt.subplot(312)
    plt.plot(simout[:, 1])
    plt.title('Elbow Velocity')
    plt.ylabel('[deg/s]')
    plt.subplot(313)
    plt.plot(simout[:, 2])
    plt.title('Elbow Acceleration')
    plt.ylabel('[deg/s^2]')
    plt.xlabel('instants')
    '''
    if rewards is not None:
        plt.figure()
        plt.plot(rewards)
        plt.title('Sum of Collected Rewards')
        plt.ylabel('Reward')
        plt.xlabel('# Episode')

    plt.show()




def multip_plotter(simout, targets, rewards=None):

    plt.figure()
    plt.subplot(211)
    plt.plot(simout[:, 0], 'g')  # , label='Actual Position')
    plt.title('Elbow Position')
    plt.ylim(50, 180)
    plt.ylabel('Angle [deg]')
    plt.hold(True)
    plt.plot(targets, 'black')  # , label='Reference Position')
    plt.legend()

    plt.subplot(212)
    plt.plot(simout[:, 3], 'r', label='PW on ch1')
    plt.plot(simout[:, 4], 'b', label='PW on ch2')
    plt.title('PW')
    plt.ylim(-5, 360)
    plt.xlabel('instants')
    plt.ylabel('PW [\mus]')
    plt.legend()

    if rewards is not None:
        plt.figure()
        plt.plot(rewards)
        plt.title('Sum of Collected Rewards')
        plt.ylabel('Reward')
        plt.xlabel('# Episode')

    plt.show()
