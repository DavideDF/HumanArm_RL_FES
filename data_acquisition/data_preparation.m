%% DATA PREPARATION

% -------------------------------------------------------------------------
% load the raw data matrix (simout.m) and run the script to pre-process the 
% data and create the datasets for trainig the biceps and triceps models.
% The function will output two datasets (bicDataset and tricDataset) in csv
% format. The columns of the dataset matricies contains the following
% signals:
% - - - - - - - - - - - - - - - inputs:
% 1 - elbow angular position
% 2 - elbow angular speed
% 3 - elbow angular acceleration
% 4 - stimulation history of the channel
% 5 - stimulation on the channel
% - - - - - - - - - - - - - - - targets:
% 6 - next elbow angular position
% 7 - next elbow angular speed
% 8 - next elbow angular acceleration
% note: simout.m should be a column matrix.
% -------------------------------------------------------------------------



% Get the useful signals
elbow_angle=simout(:,1);
current_ch1=simout(:,4);
current_ch2=simout(:,5);
vol_emg_ch1=simout(:,8);
vol_emg_ch2=simout(:,9);
N=length(elbow_angle); 

% Remove negative stimulation values, if any.
ZERO1=find(current_ch2<0);
current_ch2(ZERO1)=0;
ZERO2=find(current_ch1<0);
current_ch1(ZERO2)=0;

clear simout ZERO1 ZERO2



%% Filtering

% Filtering and derivations of the elbow angular position.
elbow_angle=filter([1/4 1/4 1/4 1/4],1,elbow_angle);
for ii=1:N-1    
    velocity(ii)=(elbow_angle(ii+1)-elbow_angle(ii))*0.04;
end
velocity=velocity';
velocity=cat(1,velocity,velocity(end));
velocity=filter([1/4 1/4 1/4 1/4],1,velocity);
for ii=1:N-1    
    acceleration(ii)=(velocity(ii+1)-velocity(ii))*0.04;
end
acceleration=acceleration';
acceleration=cat(1,acceleration,acceleration(end));
acceleration=filter([1/4 1/4 1/4 1/4],1,acceleration);

clear ii

% Volitional emg LP filter.
lpFilt=designfilt('lowpassfir','PassbandFrequency',0.05,'StopbandFrequency',0.10,'PassbandRipple',0.5,'StopbandAttenuation',65,'DesignMethod','kaiserwin');
% (put to zero NaN values, if any).
[row]=find(isnan(vol_emg_ch1));
vol_emg_ch1(row)=0;
clear row
[row]=find(isnan(vol_emg_ch2));
vol_emg_ch2(row)=0;
clear row
vol_emg_ch1=filtfilt(lpFilt,vol_emg_ch1);
vol_emg_ch2=filtfilt(lpFilt,vol_emg_ch2);

clear lpFilt



%% Fatigue signals creation

% fatigue 1
for ii=1:N
    fat1_ch1(ii)=sum(current_ch1(1:ii));
    fat1_ch2(ii)=sum(current_ch2(1:ii));
end
% fatigue 2
sumCH1=0;
sumCH2=0;
for ii=1:length(current_ch1)
    fat2_ch1(ii)=sumCH1+current_ch1(ii);
    sumCH1=fat2_ch1(ii);
    if current_ch1(ii)==0
        sumCH1 = 0;
    end
    fat2_ch2(ii)=sumCH2+current_ch2(ii);
    sumCH2=fat2_ch2(ii);
    if current_ch2(ii)==0
        sumCH2 = 0;
    end
end

clear  ii sumCH1 sumCH2



%% Total dataset creation (inputs only).
dataset=zeros(N,7);
dataset(:,1)=elbow_angle;
dataset(:,2)=velocity;
dataset(:,3)=acceleration;
dataset(:,4)=fat2_ch1;
dataset(:,5)=fat2_ch2;
dataset(:,6)=current_ch1;
dataset(:,7)=current_ch2;

clear elbow_angle velocity acceleration vol_emg_ch1 vol_emg_ch2 N fat1_ch1 fat2_ch1 fat1_ch2 fat2_ch2 current_ch1 current_ch2


%% Split the dataset into Biceps and Triceps datasets.
% Keep the signals only when the stimulation is on.

% Allocation of all the stimulating pulses (in dataset format: in+targets)
bicDataset = [];
tricDataset = [];
% Allocation of a single stimulating pulse (in dataset format: in+targets)%
datasetBic = [];
datasetTric = [];

targets = [];

for ii=1:length(dataset)-1
    %% Channel 1: Biceps muscle
    % Check when the stimulation is on
    if dataset(ii,6)~=0
        % Save all the signals at time ii
        datasetBic=[datasetBic; dataset(ii,:)];
        % Check when the stimulation pulse ends
        if dataset(ii+1,6)==0
            % Ideality 1: No backward movement with constant stimulation.
            for cc=1:length(datasetBic)-1
                if datasetBic(cc+1,1)>datasetBic(cc,1) && datasetBic(cc+1,5)>=datasetBic(cc,5)
                    datasetBic(cc+1,1)=datasetBic(cc,1);
                end
            end
            
            % Extract targets from the signals
            targets=datasetBic(2:end,[1 2 3]);
            targets=cat(1,targets,targets(end,:));
            % Built the dataset format
            datasetBic=cat(2,datasetBic,targets);
            % Save the new pulse in the complete dataset
            bicDataset=[bicDataset; datasetBic];
            datasetBic=[];
        end
    end
    
    %% Channel 2: Triceps muscle
    if dataset(ii,7)~=0
        datasetTric=[datasetTric; dataset(ii,:)];
        if dataset(ii+1,7)==0
            % Ideality 1: No backward movement with constant stimulation.
            for cc=1:length(datasetTric)-1
                if datasetTric(cc+1,1)<datasetTric(cc,1) && datasetTric(cc+1,5)>=datasetTric(cc,5)
                    datasetTric(cc+1,1)=datasetTric(cc,1);
                end
            end
            
            targets=datasetTric(2:end,[1 2 3]);
            targets=cat(1,targets,targets(end,:));
            datasetTric=cat(2,datasetTric,targets);
            tricDataset=[tricDataset; datasetTric];
            datasetTric=[];
        end
    end
    
end

% Remove signals of the other channel
bicDataset(:,[5 7])=[];
tricDataset(:,[4 6])=[];

clear datasetBic datasetTric ii targets cc

% Save as csv files
bicDataset_c=mat2dataset(bicDataset);
tricDataset_c=mat2dataset(tricDataset);
export(bicDataset_c, 'File', 'bicDataset.csv', 'Delimiter',',')
export(tricDataset_c, 'File', 'tricDataset.csv', 'Delimiter',',')
