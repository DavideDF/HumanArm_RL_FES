**HumanArm RL FES** is a Python/Matlab environment for developing reinforcement learning (RL) controllers for functional electrical stimulation (FES) of the upper limb.
Here, a single degree of freedom human arm model is considered and the electrical stimulation is controlled through pulse width modulation to activate the biceps (ch1) and he tricpes (ch2) muscles.
The continuous control problem of planar reaching is solved by using the Proximal Policy Optimizaion RL algorithm, in rllab (https://rllab.readthedocs.io).
The learning task is defined by an initial angular position of the elbow and a set of target angular positions.

The environment, built using OpenAi Gym (https://gym.openai.com/), contains the human arm model and two different I/O model of the human arm under electrical stimulation can be used to simulate episodes.

***


Requirements
============

Python 2.7 --> https://www.python.org/download/releases/2.7/

you need to install:

Open Ai Gym --> https://github.com/openai/gym

rllab --> https://github.com/rll/rllab

Add in the rllab-master directory the scritps in the ``rllab_master_plugins`` folder:

    human_muscles.py in /rllab-master/rllab/envs
    multipleSetPoint_PPO.py in /rllab-master/examples/
    
Matlab & Simulink --> https://it.mathworks.com


Collecting Experintal Data
======================
Launch the simulink program ``/dataAcquisition/dataAcquisitionProtocol.slx`` to conduct an experiment with a real subject using and adequante experimtal setup.
After the protocol execution, you can process the obtained data, stored in ``/dataAcquisition/DATA``, by loading the file and running the script ``/dataAcquisition/data_preparation.m``. 
Two csv files will be created, containing the dataset of each single muscle to train the human arm model.


Create a new Human Arm Model
======================
Work in progress...

Set up an Experiment
======================
Use the script ``multipleSetPoint_PPO.py`` to set and run the learning experiment.

Settings

Parameters:

    initial_position = the starting elbow angle for each episode.
    set_of_targets = the list of the target position to reach during the learning.
    pw_range = [string] the interval of pulse width modulation. It can be '0350', '100350' and '250350'.
    nn = the number of hidden layers units of the MLP policy estimator.
    episode_duration = the duration of a simulated episode.
    n_eps_x_iteration = batch size (number of simulated episodes per batch).

Selection of the Human Arm Model: you can choose between two different ANN model of he human arm by comment/uncomment the line with the model name.


Simulate the Policy 
=======================
The learned policy is saved in the ``/rl_fes/policies/`` folder in ``.mat`` format. You can use he Matlab function ``/rl_fes/lasagne2keras.m`` to convert and save the policy parameters in the ``rl_fes`` directory.
To simulate he policy, use the ``/rl_fes/simulator.py`` script, setting the following parametes:

    set the number of hidden units of the MLP policy stimator (nn)
    select the model used during the training (by comment/uncomment the line with the model name)
    set the initial and the target positions
    set the pulse width modulation interval used during the training
    

Increase the Generalization Ability of the Policy 
===================================================
You can train more general policy by randomly change the initial conditions at the beginning of each batch of training.
To add this feature uncomment from line 59 to 64 in the ``multipleSetPoint_PPO.py`` code.

